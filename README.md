# login (JS app)


## How to run app?
Open index.html in your browser

## How to test app? 
1. Enter username and password
1. hit login button.

> If user not registered already then please add another user in `users.json` file. (`data/users.json`)


## How to add a new user? 
1. open `data` folder
1. add `{"username":"test", "password":"1234"}`
2. save the file
3. Now, reload the app in your browser and try with the new (test) user