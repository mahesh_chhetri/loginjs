/**
 * Responsible for login user
 * Checkes if user already registered or not
 */
$(function () {
    $("#loginForm").on("submit", function (e) {
        e.preventDefault();
        var username = $("#username").val();
        var password = $("#password").val();

        function hasRegisteredUser(data) {
            return data.find(function (user) {
                return (user.username == username && user.password == password);
            });
        }
        // on success respopnse
        function onSuccess(res) {
            var result = hasRegisteredUser(res);
            toggleMessage(result);
        }
        // on error response
        function onError(err) {
            // do other programme here
            console.log("On Error", err);
        }
        // Requesting backend
        requestBackend(onSuccess, onError);
    });
});