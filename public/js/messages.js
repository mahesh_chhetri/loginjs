function toggleMessage(result) {
    if (result) {
        $("#error").addClass("d-none");
        $("#success").removeClass('d-none');
    } else {
        $("#error").removeClass('d-none');
        $("#success").addClass('d-none');
    }
}