function requestBackend(successCallback, errorCallback) {
    $.get({
        url: "../data/users.json",
        success: successCallback,
        error: errorCallback
    });
}